
# QA Automation Test
## Willy Osorto

This is the main repository that contains the Automation Scripts to complete the test




## Run Tests

Please run the next commands on your terminal in order to download the project and install its dependencies

```bash
  git clone https://gitlab.com/wodevs/qa-assessment-willyosorto.git
```

Go to the project directory

```bash
  cd qa-assessment-willyosorto
```

Install dependencies

```bash
  npm install
```

Run the tests using the Cypress Runner UI

```bash
  npm run cy:open
```
This will open the Cypress Runner UI, to run the tests locally you can follow the next steps:

#### Select the Testing Type - E2E Testing ####
![App Screenshot](https://firebasestorage.googleapis.com/v0/b/wollet-app.appspot.com/o/temp-folder%2FXnip2024-06-17_11-13-27.jpg?alt=media&token=7827c72e-f959-454b-936e-7a14a4f35606)

#### Pick your preferred browser ####
![App Screenshot](https://firebasestorage.googleapis.com/v0/b/wollet-app.appspot.com/o/temp-folder%2FXnip2024-06-17_11-13-40.jpg?alt=media&token=1063d1d9-bb30-4faa-bdeb-0a3195f57dcf)

#### Run the tests ####
![App Screenshot](https://firebasestorage.googleapis.com/v0/b/wollet-app.appspot.com/o/temp-folder%2FXnip2024-06-17_11-14-00.jpg?alt=media&token=76c463ea-a8a8-45dd-b9d3-d9540783b5bf)

Run the tests using in headless mode

```bash
  npm run cy:run
```


## Generating Reports

To generate a HTML report, please run the next commands in your terminal

Run the tests using in headless mode

```bash
  npm run cy:run
```

Merge all the specs results reports in a single one

```bash
  npm run report:merge
```

Generate the global report

```bash
  npm run report:generate
```

Open the report

```bash
  npm run report:open
```

## Authors

- [@wodevs](https://gitlab.com/wodevs)

