const { defineConfig } = require("cypress");

module.exports = defineConfig({
  reporter: 'mochawesome',
  reporterOptions: {
    useInlineDiffs: true,
    embeddedScreenshots: true,
    reportDir: 'cypress/results',
    reportFilename: '[name].html',
    overwrite: true,
    html: true,
    json: true,
  },
  e2e: {
    experimentalRunAllSpecs: true,
    baseUrl: 'https://automationexercise.com',
    viewportWidth: 1440,
    viewportHeight: 900,
    specPattern: [
      '**/home.cy.js',
      '**/authentication.cy.js',
      '**/products.cy.js',
      '**/cart.cy.js',
      '**/mainflow.cy.js',
    ],
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
  env: {
    noUserEmail: 'nonexistentuser@email.com',
    noUserPass: 'NoexistentP@ssword',
    userEmail: 'willy@email.com',
    userPassword: 'P@ssw0rd1990$',
    userFullName: 'Willy Osorto',
    userBirthday: '18-6-1990',     
    userAddress: '7800 SW 127TH DR',
    userCountry: 'United States',
    userState: 'Florida',
    userCity: 'Miami',
    userZipCode: '33183-4290',
    userPhoneNumber: '+1 (305) 873-4256',
  },
});
