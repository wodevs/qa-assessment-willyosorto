import selectors from "../support/selectors"

describe('Authentication Tests', () => {
  beforeEach(() => {
    cy.visit('/login')
  })

  it('Check non-existent user login', () => {
    cy.get(selectors.auth.loginPageTitle).contains('Login to your account')
    cy.get(selectors.auth.emailInput).type(Cypress.env('noUserEmail'))
    cy.get(selectors.auth.passwordInput).type(Cypress.env('noUserPass'))
    cy.get(selectors.auth.loginButton).click()
    cy.get(selectors.auth.loginErrorText).contains('Your email or password is incorrect!')
  })  
  
  it('Create new account', () => {
    cy.get(selectors.auth.registerPageTitle).contains('New User Signup!')
    cy.get(selectors.auth.registerUserNameInput).type(Cypress.env('userFullName'))
    cy.get(selectors.auth.registerUserEmailInput).type(Cypress.env('userEmail'))
    cy.get(selectors.auth.registerButton).click()
    cy.get(selectors.auth.registerFormTitle).contains('Enter Account Information')
    cy.get(selectors.auth.registerFormUserTitle).check()
    cy.get(selectors.auth.registerFormUserPassword).type(Cypress.env('userPassword'))
    cy.get(selectors.auth.registerFormUserBDayDate).select(Cypress.env('userBirthday').split('-')[0])
    cy.get(selectors.auth.registerFormUserBDayMonth).select(Cypress.env('userBirthday').split('-')[1])
    cy.get(selectors.auth.registerFormUserBDayYear).select(Cypress.env('userBirthday').split('-')[2])
    cy.get(selectors.auth.registerFormUserFName).type(Cypress.env('userFullName').split(' ')[0])
    cy.get(selectors.auth.registerFormUserFLame).type(Cypress.env('userFullName').split(' ')[1])
    cy.get(selectors.auth.registerFormUserAddress).type(Cypress.env('userAddress'))
    cy.get(selectors.auth.registerFormUserCountry).select(Cypress.env('userCountry'))
    cy.get(selectors.auth.registerFormUserState).type(Cypress.env('userState'))
    cy.get(selectors.auth.registerFormUserCity).type(Cypress.env('userCity'))
    cy.get(selectors.auth.registerFormUserZipCode).type(Cypress.env('userZipCode'))
    cy.get(selectors.auth.registerFormUserPhone).type(Cypress.env('userPhoneNumber'))
    cy.get(selectors.auth.registerFormCreateButton).click()
    cy.get(selectors.auth.registerConfirmationText).contains('Congratulations! Your new account has been successfully created!')
    cy.get(selectors.auth.registerConfirmationContinueButton).click()
    cy.get(selectors.auth.loggedUserTitle).contains(`Logged in as ${Cypress.env('userFullName')}`)
  })

  it('Log In and Log Out', () => {
    cy.get(selectors.auth.loginPageTitle).contains('Login to your account')
    cy.get(selectors.auth.emailInput).type(Cypress.env('userEmail'))
    cy.get(selectors.auth.passwordInput).type(Cypress.env('userPassword'))
    cy.get(selectors.auth.loginButton).click()
    cy.get(selectors.auth.loggedUserTitle).contains(`Logged in as ${Cypress.env('userFullName')}`)
    cy.get(selectors.auth.logOutButton).click()
    cy.get(selectors.auth.loginPageTitle).should('exist')
  })

  it('Delete Account', () => {
    cy.get(selectors.auth.loginPageTitle).contains('Login to your account')
    cy.get(selectors.auth.emailInput).type(Cypress.env('userEmail'))
    cy.get(selectors.auth.passwordInput).type(Cypress.env('userPassword'))
    cy.get(selectors.auth.loginButton).click()
    cy.get(selectors.auth.loggedUserTitle).contains(`Logged in as ${Cypress.env('userFullName')}`)
    cy.get(selectors.auth.deleteAccountButton).click()
    cy.get(selectors.auth.accountDeletedText).should('exist')
    cy.get(selectors.auth.accountDeleteContinueButton).click()
  })
})