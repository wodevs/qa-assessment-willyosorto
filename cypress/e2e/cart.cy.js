import selectors from "../support/selectors"

describe('Cart Tests', () => {
  beforeEach(() => {
    cy.visit('/products')
  })

  it('Check product quantity', () => {
    cy.get(selectors.products.inputSearch).type('Sleeveless Dress')
    cy.get(selectors.products.submitSearchButton).click()
    cy.get(selectors.products.productIntoTitle).contains('Sleeveless Dress')
    cy.get(selectors.products.productViewButton).click()
    cy.get(selectors.products.productQuantity).clear().type(10)
    cy.get(selectors.products.productAddToCartButton).click()
    cy.get(selectors.products.productAddedModalTitle).contains('Added!')
    cy.get(selectors.products.productAddedModalViewCartBtn).children().click()
    cy.get(selectors.cart.cartProductQuantity).children().contains(10)
  })

  it('Check cart multiple products', () => {
    cy.get(selectors.products.firstProductListed).click()
    cy.get(selectors.products.productAddedModalTitle).contains('Added!')
    cy.get(selectors.products.productAddedModalShoppingBtn).click()
    cy.get(selectors.products.secondProductListed).click()
    cy.get(selectors.products.productAddedModalTitle).contains('Added!')
    cy.get(selectors.products.productAddedModalViewCartBtn).children().click()
    cy.get(selectors.cart.cartTableBody).should('have.length', 2)
  })
})