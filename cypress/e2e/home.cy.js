import selectors from "../support/selectors"

describe('Home Tests', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('Landing pages opens', () => {
    cy.get(selectors.home.homeHeroBanner).should('exist')
  })

  it('Page scrolls over the products', () => {
    cy.scrollTo('center')
    cy.get(selectors.home.homeProductCard).should('exist')
  })
})