import { faker } from "@faker-js/faker"
import selectors from "../support/selectors"

describe('Assessment Main Flow', () => {
  //Getting fake data to run the test
  const fakeEmail = faker.internet.email();
  const fakeCreditCard = faker.finance.creditCardNumber();
  
  it('User Completes a Purchase', () => {
    cy.visit('/')
    //The user picks up a product and opens the product details
    cy.scrollTo('center')
    cy.get(selectors.home.homeProductCard).should('exist')
    cy.get(selectors.home.homeProductViewButton).click()
    //The user changes the quantity and add the product to the cart
    cy.get(selectors.products.productQuantity).clear().type(30)
    cy.get(selectors.products.productAddToCartButton).click()
    cy.get(selectors.products.productAddedModalTitle).should('have.text', 'Added!')
    //User navigates to the cart and initiates the checkout process
    cy.get(selectors.products.productAddedModalViewCartBtn).children().click()
    cy.get(selectors.cart.cartProductQuantity).children().should('have.text', 30)
    cy.get(selectors.cart.proceedCheckoutButton).click()
    cy.get(selectors.cart.modalMessageText).should('have.text', 'Register / Login account to proceed on checkout.')
    cy.get(selectors.cart.modalRegisterLoginButton).click()
    //User is redirected to the Login/Register page and start the Register process
    cy.get(selectors.auth.registerPageTitle).should('exist')
    cy.get(selectors.auth.registerUserNameInput).type(Cypress.env('userFullName'))
    cy.get(selectors.auth.registerUserEmailInput).type(fakeEmail)
    cy.get(selectors.auth.registerButton).click()
    cy.register()
    //User navigates to the cart and proceeds to checkout the order
    cy.get(selectors.home.homeHeaderMenuCart).click()
    cy.get(selectors.cart.cartProductQuantity).children().should('have.text', 30)
    cy.get(selectors.cart.proceedCheckoutButton).click()
    cy.url().should('eq', Cypress.config().baseUrl + '/checkout')
    cy.get(selectors.cart.cartTotalAmount).should('have.text', 'Rs. 45000')
    cy.get(selectors.cart.cartPlaceOrderButton).click()
    //User go to the Payment Method page and add his credit card information
    cy.get(selectors.payment.headTitle).should('exist')
    cy.get(selectors.payment.cardName).type(Cypress.env('userFullName'))    
    cy.get(selectors.payment.cardNumber).type(fakeCreditCard)
    cy.get(selectors.payment.cardCvc).type(123)
    cy.get(selectors.payment.cardMonth).type(10)
    cy.get(selectors.payment.cardYear).type(2028)
    cy.get(selectors.payment.payButton).click()
    //User is redirected to the confirmation order page
    cy.get(selectors.payment.orderConfirmationTitle).should('exist')
    cy.get(selectors.payment.orderConfirmationText).should('have.text', 'Congratulations! Your order has been confirmed!')
    cy.get(selectors.payment.orderConfirmationButton).click()
    //User logs out and logs back in
    cy.get(selectors.auth.logOutButton).click()
    cy.url().should('eq', Cypress.config().baseUrl + '/login')
    cy.get(selectors.auth.emailInput).type(fakeEmail)
    cy.get(selectors.auth.passwordInput).type(Cypress.env('userPassword'))
    cy.get(selectors.auth.loginButton).click()
    cy.get(selectors.auth.loggedUserTitle).contains(`Logged in as ${Cypress.env('userFullName')}`)
    //User navigates to Contact Us and send a message
    cy.get(selectors.home.homeHeaderContactUs).click()
    cy.get(selectors.contact.contactFormName).type(Cypress.env('userFullName'))
    cy.get(selectors.contact.contactFormEmail).type(fakeEmail)    
    cy.get(selectors.contact.contactFormText).type(faker.lorem.text())
    cy.get(selectors.contact.contactFormButton).click()
    //User closes his session
    cy.get(selectors.auth.logOutButton).click()    
  })

  it('Account Deletion', () => {
    //Deleting te user's account for data clean up
    cy.deleteAccount(fakeEmail)
  })
})