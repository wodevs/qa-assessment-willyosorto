import selectors from "../support/selectors"

describe('Products Tests', () => {
  beforeEach(() => {
    cy.visit('/products')
  })

  it('Search products input is displayed', () => {
    cy.get(selectors.products.inputSearch).should('exist')
  })

  it('Filter products by Category', () => {
    cy.get(selectors.products.womenCategory).click()
    cy.get(selectors.products.dressSubCategory).should('exist')
    cy.get(selectors.products.dressSubCategory).click()
    cy.get(selectors.products.categoryTitle).contains('Women - Dress Products')
  })

  it('Filter products by Brand', () => {    
    cy.get(selectors.products.brandPoloCategory).should('exist').click()    
    cy.get(selectors.products.categoryTitle).contains('Brand - Polo Products')
  })

  it('Check Product Details', () => {    
    cy.get(selectors.products.brandHmCategory).should('exist').click()
    cy.get(selectors.products.categoryTitle).contains('Brand - H&M Products')
    cy.get(selectors.products.tShirtProductTitle).contains('Men Tshirt')
    cy.get(selectors.products.tShirtProductViewButton).should('exist').click()
    cy.get(selectors.products.productTitle).contains('Men Tshirt')
    cy.get(selectors.products.productPrice).contains('Rs. 400')
  })

  it('Check Product Wrong Details', () => {    
    cy.get(selectors.products.brandHmCategory).should('exist').click()
    cy.get(selectors.products.categoryTitle).contains('Brand - H&M Products')
    cy.get(selectors.products.tShirtProductTitle).contains('Men Tshirt')
    cy.get(selectors.products.tShirtProductViewButton).should('exist').click()
    cy.get(selectors.products.productTitle).contains('Men Jeans')
  })
})