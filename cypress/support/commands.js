// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
import selectors from "./selectors"

Cypress.Commands.add('register', () => {
    cy.get(selectors.auth.registerFormTitle).contains('Enter Account Information')
    cy.get(selectors.auth.registerFormUserTitle).check()
    cy.get(selectors.auth.registerFormUserPassword).type(Cypress.env('userPassword'))
    cy.get(selectors.auth.registerFormUserBDayDate).select(Cypress.env('userBirthday').split('-')[0])
    cy.get(selectors.auth.registerFormUserBDayMonth).select(Cypress.env('userBirthday').split('-')[1])
    cy.get(selectors.auth.registerFormUserBDayYear).select(Cypress.env('userBirthday').split('-')[2])
    cy.get(selectors.auth.registerFormUserFName).type(Cypress.env('userFullName').split(' ')[0])
    cy.get(selectors.auth.registerFormUserFLame).type(Cypress.env('userFullName').split(' ')[1])
    cy.get(selectors.auth.registerFormUserAddress).type(Cypress.env('userAddress'))
    cy.get(selectors.auth.registerFormUserCountry).select(Cypress.env('userCountry'))
    cy.get(selectors.auth.registerFormUserState).type(Cypress.env('userState'))
    cy.get(selectors.auth.registerFormUserCity).type(Cypress.env('userCity'))
    cy.get(selectors.auth.registerFormUserZipCode).type(Cypress.env('userZipCode'))
    cy.get(selectors.auth.registerFormUserPhone).type(Cypress.env('userPhoneNumber'))
    cy.get(selectors.auth.registerFormCreateButton).click()
    cy.get(selectors.auth.registerConfirmationText).contains('Congratulations! Your new account has been successfully created!')
    cy.get(selectors.auth.registerConfirmationContinueButton).click()
    cy.get(selectors.auth.loggedUserTitle).contains(`Logged in as ${Cypress.env('userFullName')}`)
})

Cypress.Commands.add('deleteAccount', (email) => {
    cy.visit('/login')
    cy.get(selectors.auth.emailInput).type(email)
    cy.get(selectors.auth.passwordInput).type(Cypress.env('userPassword'))
    cy.get(selectors.auth.loginButton).click()
    cy.get(selectors.auth.loggedUserTitle).contains(`Logged in as ${Cypress.env('userFullName')}`)
    cy.get(selectors.auth.deleteAccountButton).click()
    cy.get(selectors.auth.accountDeletedText).should('exist')
})